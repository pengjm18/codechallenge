/**
 * @jest-environment jsdom
 */

import {sum_to_n_a, sum_to_n_b, sum_to_n_c} from './index';

describe('Three ways summation to n', () => {
  test('Shows valid results if the sums are correct', () => {
    const datas = [
      {inputNumber: 5, expectedResult: 15},
      {inputNumber: 6, expectedResult: 21},
      {inputNumber: 7, expectedResult: 28},
    ];

    expect.assertions(datas.length);

    return Promise.all(datas.map(async (data) => {
      expect(sum_to_n_a(data.inputNumber)).toEqual(data.expectedResult);
      expect(sum_to_n_b(data.inputNumber)).toEqual(data.expectedResult);
      expect(sum_to_n_c(data.inputNumber)).toEqual(data.expectedResult);
    }));
  });
});