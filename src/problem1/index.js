/*
 *  Provide 3 unique implementations of the following function.
 *  Input: n - any integer
 *  Assuming this input will always produce a result lesser than
 *  Number.MAX_SAFE_INTEGER. Output: return - summation to n, i.e. sum_to_n(5)
 *  === 1 + 2 + 3 + 4 + 5 === 15.
 */

var sum_to_n_a = function(n) {
  let count = 0;
  let result = 0;
  while (count <= n) {
    result += count;
    count++;
  }
  return result;
};

var sum_to_n_b = function(n) {
  let result = 0;
  for (let count = 0; count <= n; count++) {
    result += count;
  }
  return result;
};

var sum_to_n_c = function(n) {
  const result = (n / 2) * (1 + n);
  return result;
};