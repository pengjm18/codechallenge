(function(customAlertBox) {
'use strict';

const inputAddressText = document.getElementById('inputAddressText');
const inputAmountText = document.getElementById('inputAmountText');
const inputOTPText = document.getElementById('inputOTPText');

const appendTransactionStatus = (function() {
  return function(text) {
    document.getElementById('transactionStatusText').textContent = text;
  };
}());

const handleInputAddress = (function() {
  return function() {
    if (!Web3.utils.isAddress(inputAddressText.value)) {
      appendTransactionStatus('ETH address is incorrect. Please try again.');
      inputAddressText.value = '';
      return false;
    } else {
      appendTransactionStatus('');
    }
  }
}());

const handleInputAmount = (function() {
  return function() {
    if (inputAmountText.value === '') {
      appendTransactionStatus('Please enter a valid ETH amount.');
      return false;
    } else {
      appendTransactionStatus('');
    }
  }
}());

const handleSendButton = (function() {
  return function() {
    if (!Web3.utils.isAddress(inputAddressText.value)) {
      appendTransactionStatus('ETH address is incorrect. Please try again.');
      inputAddressText.value = '';
      return false;
    }

    if (inputAmountText.value === '') {
      appendTransactionStatus('Please enter a valid ETH amount.');
      return false;
    }

    if (inputOTPText.value === '123') {
      customAlertBox(
          `<span class='customAlertMessage'>${
              inputAmountText.value} ETH has been sent successfully to ${
              inputAddressText.value}</span>`,
          'Receipt', 'OK');
    } else {
      appendTransactionStatus('Please enter OTP number 123 to proceed.');
      inputOTPText.value = '';
      return false;
    }
  }
}());

(function initInputAddressText() {
  inputAddressText.addEventListener(
      'change', function handleInputAddressChange(event) {
        event.preventDefault();
        handleInputAddress();
      }, false);
}());

(function initinputAmountText() {
  inputAmountText.addEventListener(
      'change', function handleInputAmountChange(event) {
        event.preventDefault();
        handleInputAmount();
      }, false);
}());

(function initSubmitButton() {
  document.getElementById('sendButton')
      .addEventListener('click', function handleSubmitButtonClick(event) {
        event.preventDefault();
        handleSendButton();
      }, false);
}());
}(window.customAlertBox));