window.customAlertBox = (function($) {
  'use strict';

  return function(message, title, buttonText) {
    buttonText = (buttonText == undefined) ? 'Ok' : buttonText;
    title = (title == undefined) ? 'The page says:' : title;

    var div = $('<div>');
    div.html(message);
    div.attr('title', title);
    div.dialog({
      autoOpen: true,
      modal: true,
      draggable: false,
      resizable: false,
      width: 'auto',
      buttons: [{
        text: buttonText,
        click: function() {
          $(this).dialog('close');
          div.remove();
          location.reload();
        }
      }]
    });
  }
}(window.$));
