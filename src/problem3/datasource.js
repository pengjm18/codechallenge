'use strict';

const url = 'https://static.ngnrs.io/test/prices';

function Datasource() {}

Datasource.prototype.getPrices = function(timeout) {
  return new Promise(function(resolve, reject) {
    const xhr = new XMLHttpRequest();

    xhr.timeout = (timeout === undefined ? 5000 : timeout);

    const asynchronous = true;
    xhr.open('GET', url, asynchronous);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onload = function() {
      if (this.readyState === 4) {
        resolve(JSON.parse(this.responseText).data.prices);
      } else {
        reject(this.statusText);
      }
    };

    xhr.ontimeout = function() {
      reject(new Error('Get prices request timeout'));
    };

    xhr.onerror = function(error) {
      reject(error);
    };

    xhr.send();
  });
};

Datasource.prototype.getMidPriceValue = function(price) {
  let midPointValue = price.buy - price.sell;
  return midPointValue;
};

Datasource.prototype.getQuoteCurrency = function(quote) {
  return quote.pair.substring(3);
};