/*
 *  Implement a datasource connector to abstract away data retrieval and
 *  manipulation from the View Controllers. You are required to implement a
 *  Datasource utility class.
 */

'use strict';

let ds = new Datasource();

ds.getPrices()
    .then(prices => {
      prices.forEach(price => {
        console.log(`Mid price for ${price.pair} is ${
            ds.getMidPriceValue(price)} ${ds.getQuoteCurrency(price)}.`);
      });
    })
    .catch(error => {
      console.err(error);
    });